# Injectable

Dead simple dependency injection for NodeJS Modules.

*Usage*
```
const Injectable = require('injectable');

ExampleModuleFactory.$inject = ['dependencyA','dependencyB'];
function ExampleModuleFactory(inject){

    return {
        echo: echo
    }

    function echo(input){
        return inject.dependencyA.echo() + " " + inject.dependencyB.echo();
    }
}

module.exports = new Injectable(ExampleModuleFactory);

```
Then to use it in another module:
```
const dependencyA = { echo: () => "dependencyA" };
const dependencyB = { echo: () => "dependencyB" };
const ExampleModule = require('./ExampleModule')({
    dependencyA: dependencyA,
    dependencyB: dependencyB
});

ExampleModule.echo();
```
