'use strict';
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require("sinon-chai");
chai.use(sinonChai);
const expect = chai.expect;

const dependencyA = { echo: () => "dependencyA" };
const dependencyB = { echo: () => "dependencyB" };
const ExampleModule = require('./ExampleModule')({
    dependencyA: dependencyA,
    dependencyB: dependencyB
});

describe('ExampleModule',function(){

    it('should allow injection', function(){
        const result = ExampleModule.echo("test");
        expect(result).to.equal("dependencyA dependencyB");
    })
});
