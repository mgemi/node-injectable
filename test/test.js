'use strict';
const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require("sinon-chai");
chai.use(sinonChai);
const expect = chai.expect;

const Injectable = require('../');

describe('Injectable',function(){

    MyModule.$inject = ['somevalue',"someothervalue"];
    function MyModule(dependencies){
        return dependencies
    }

    let testModule
    before(function(){
        testModule = new Injectable(MyModule);
    })

    it('should inject dependencies',function(){
        const mod = testModule({
            somevalue: "foo",
            someothervalue: "bar"
        });

        expect(mod.somevalue).to.equal("foo");
        expect(mod.someothervalue).to.equal("bar");
    });

    it('should throw if missing dependencies', function(){
        expect(function(){
            testModule({ somevalue: "foo", extra: "bar" }) // missing someothervalue
        }).to.throw(Error,'Expecting dependency `someothervalue` to have been injected, but received `somevalue,extra`');
    })

});
