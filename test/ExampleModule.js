const Injectable = require('../');

ExampleModuleFactory.$inject = ['dependencyA','dependencyB'];
function ExampleModuleFactory(inject){

    return {
        echo: echo
    }

    function echo(input){
        return inject.dependencyA.echo() + " " + inject.dependencyB.echo();
    }
}

module.exports = new Injectable(ExampleModuleFactory);
