module.exports = function(f){
    if(!f.$inject) throw new Error("No Dependencies specified - add module.$inject to your module");
    return function(dependencies){
        validateInjection(f.$inject, dependencies);
        return f(dependencies);
    }
}

function validateInjection(requested, dependencies){
    requested.forEach( dep => {
        if(!dependencies[dep]) throw new Error('Expecting dependency `' + dep + '` to have been injected, but received `' + Object.keys(dependencies) + "`" );
    });
}
